import nni
import logging
import pandas as pd
import numpy as np
from xgboost import XGBClassifier
from sklearn import model_selection

LOG = logging.getLogger('xgb-demo')


def load_data():
    '''Load dataset'''
    data_train = pd.read_csv("fake_data.csv")  # 加载训练数据
    features = ["feat_1", "feat_2", "feat_3"]
    label = ["label"]

    X, y = data_train[features], data_train[label]
    return X, y


def get_default_parameters():
    '''get default parameters'''
    params = {
        'max_depth': 3,
        'min_child_weight': 1,
        'gamma': 0,
        'subsample': 0.8,
        'colsample_bytree': 0.8,
        'reg_alpha': 0,
        'learning_rate': 0.1
    }
    return params


def get_model(PARAMS):
    model = XGBClassifier(booster='gbtree', silent=True, nthread=None, random_state=42, base_score=0.5,
                          colsample_bylevel=1, n_estimators=100, reg_lambda=1, objective='binary:logistic')
    model.max_depth = PARAMS.get("max_depth")
    model.min_child_weight = PARAMS.get("min_child_weight")
    model.gamma = PARAMS.get("gamma")
    model.subsample = PARAMS.get("subsample")
    model.colsample_bytree = PARAMS.get("colsample_bytree")
    model.reg_alpha = PARAMS.get('reg_alpha')
    model.learning_rate = PARAMS.get("learning_rate")

    return model


def save_model_s3(model, unique_id, score, s3_bucket, s3_prefix):
    """
    跟之前保存的模型对比，如果比以前的结果都好，则保存模型
    """
    import s3fs
    from sklearn.externals import joblib

    s3 = s3fs.S3FileSystem(anon=False)
    rt = s3.ls(f"{s3_bucket}/{s3_prefix}")

    if len(rt) > 0:
        max_score = max([float(m.split('|')[-1].split('.')[0].replace('_', '.')) for m in rt])
        save_model = score >= max_score
    else:
        save_model = True

    if save_model:
        with s3.open(f"s3://{s3_bucket}/{s3_prefix}/{unique_id}|{str(score).replace('.', '_')}.pkl", "wb") as f:
            joblib.dump(model, f)


def run(X_train, y_train, model):
    '''Train model and predict result'''
    kf = model_selection.KFold(n_splits=10, shuffle=False, random_state=42)  # 10折交叉验证
    scores = model_selection.cross_val_score(model, X_train, y_train, cv=kf, scoring='accuracy')
    print(scores)
    score = scores.mean()
    LOG.debug('score: %s' % score)
    nni.report_final_result(score)  # 向 NNI 报告本次实验的结果

    save_model_s3(model, nni.get_trial_id(), score, "vision.algo.data", f"tyu/nni/{nni.get_experiment_id()}")


if __name__ == '__main__':
    X_train, y_train = load_data()
    try:
        RECEIVED_PARAMS = nni.get_next_parameter()  # 从 NNI tuner 处获得本次 trial 的参数
        LOG.debug(RECEIVED_PARAMS)
        PARAMS = get_default_parameters()
        PARAMS.update(RECEIVED_PARAMS)
        LOG.debug(PARAMS)
        model = get_model(PARAMS)
        run(X_train, y_train, model)
    except Exception as exception:
        LOG.exception(exception)
        raise
